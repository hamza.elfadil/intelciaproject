package ma.test.projet.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Etablissement {

    @JsonProperty("siret")
    private String siret;

    @JsonProperty("nic")
    private String nic;
    
    @JsonProperty("fullAdress")
    private String fullAdress;
    
    @JsonProperty("dateCreation")
    private String dateCreation;
    
    @JsonProperty("fullName")
    private String fullName;
}
