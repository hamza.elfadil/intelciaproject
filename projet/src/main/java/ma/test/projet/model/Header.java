package ma.test.projet.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Header {

    @JsonProperty("message")
    private String message;

    @JsonProperty("statut")
    private String statut;
}
