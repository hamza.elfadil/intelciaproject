package ma.test.projet.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Entreprises {

   private String id;

   @JsonProperty("header")
   private Header header;

   @JsonProperty("etablissement")
   private Etablissement etablissement ;

}
