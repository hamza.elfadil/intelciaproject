package ma.test.projet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.test.projet.dto.Response;
import ma.test.projet.serviceImpl.EntrepriseServiceImpl;

@RestController
@RequestMapping("api/entreprises")
public class EntrepriseController {

    @Autowired
    private EntrepriseServiceImpl entrepriseServiceImpl;

    @GetMapping("/json")
    public Response findResultJson() throws Exception{
        return entrepriseServiceImpl.findResultJson();
    }

    
}
