package ma.test.projet.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ma.test.projet.model.Etablissement;
import ma.test.projet.model.Header;

@Getter
@Setter
@ToString
public class Response {
    Header header;
    Etablissement etablissement;
}
