package ma.test.projet.serviceImpl;

import java.io.FileWriter;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.opencsv.CSVWriter;

import ma.test.projet.Iservice.EntreprisesService;
import ma.test.projet.dto.Response;

@Service
public class EntrepriseServiceImpl implements EntreprisesService {

    String url="https://run.mocky.io/v3/fd161e5e-eb54-4bbb-ae04-bb819a8ed66d";

    @Override
    public Response findResultJson() throws Exception {

        //String url = "https://run.mocky.io/v3/30ae9059-bcaf-4e74-9653-ec232debdd0b";
        RestTemplate restTemplate = new RestTemplate();

        Response result = restTemplate.getForObject(url, Response.class);
            
        FileWriter writer = new FileWriter("C:\\Users\\Hamza\\Desktop\\"+result.getEtablissement().getSiret()+".csv");

        CSVWriter csvWriter = new CSVWriter(writer);

        //adding header to csv
        String[] header = { "Siret", "Nic", "Full Adress" ,"Création Date","Full Name"};
        csvWriter.writeNext(header);

        // add data to csv
        String[] data = { result.getEtablissement().getSiret(), result.getEtablissement().getNic(),result.getEtablissement().getFullAdress(),result.getEtablissement().getDateCreation(),result.getEtablissement().getFullName() };
        csvWriter.writeNext(data);

        csvWriter.close();
        
        return result;
    }

}
